#define PREV 13
#define BLED 7
#define RLED 6
#define PUMP 9
#define PMTR 10
#define PFAN 4

void setup()
{
  pinMode(PREV, OUTPUT);
  pinMode(BLED, OUTPUT);
  pinMode(RLED, OUTPUT);
  pinMode(PUMP, OUTPUT);
  pinMode(PMTR, OUTPUT);
  pinMode(PFAN, OUTPUT);
  pinMode(3, OUTPUT);
}

void loop()
{
  digitalWrite(PFAN, HIGH);
  delay(5000);
  digitalWrite(PFAN, LOW);
  delay(5000);
}
